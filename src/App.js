import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import { Select,Segment } from 'semantic-ui-react'
import {map,clone} from 'underscore';
export default class App extends React.Component {
  state = {
    inputGrid: 10,
    numberOfMines: 20,
    gameOver: false,
    refreshTable: false,
    isTest:false,
    mines: [],
    isWinner:false,
    tableData:{},
    isIntialise:false
  }

  initialise = () => {
    let table = this.initialiseTable();

    let minesIndex = [];
    for (var i = 0; i < this.state.numberOfMines; i++) {
      var row = Math.floor(Math.random() * this.state.inputGrid);
      var col = Math.floor(Math.random() * this.state.inputGrid);
      table[row][col].isMines = true;
      table[row][col].className="mines";
    }
    return table;
  }

  initialiseTable = () =>{
    let tableData = {};
    Array.from({ length: this.state.inputGrid }).map((_, rowIndex) => {
      tableData[rowIndex]={};
      Array.from({ length: this.state.inputGrid }).map((_, colIndex) => {
        tableData[rowIndex][colIndex] = {
          val:-1,
          className:"",
          isMines:false
        }
      })
    });
    console.log(tableData);
    return tableData;
  }

  getMinesEle = (rowIndex, colIndex,colData) => {
    return (
      <td index={rowIndex + "" + colIndex} className={colData.className} data-mines onClick={this.cellClick} data-index={[rowIndex, colIndex]}></td>
    )
  }

  cellClick = (e) => {
    if (this.state.gameOver) {
      return;
    }
    if (e.currentTarget?.attributes?.hasOwnProperty("data-mines")) {
      let currentClass = e.currentTarget.className;
      e.currentTarget.className = currentClass + " clicked"
      this.showMines(e);
    } else {
      let cellIndex = e.currentTarget?.attributes?.["data-index"]?.value.split(",");
      let tableData = clone(this.state.tableData);
      let tableDataUpdated = this.clickNonMines(parseInt(cellIndex[0]),parseInt(cellIndex[1]),tableData);
      this.setState({
        tableData:tableDataUpdated
      },()=>{
        this.checkWinner();
      })
    }
  }

  clickNonMines = (rowIndex,colIndex,tableData)=>{
    let mineCount=0;
    for (var i=Math.max(rowIndex-1,0); i<=Math.min(rowIndex+1,this.state.inputGrid-1); i++) {
      for(var j=Math.max(colIndex-1,0); j<=Math.min(colIndex+1,this.state.inputGrid-1); j++) {
        if (tableData[i][j].isMines) mineCount++;
      }
    }

    tableData[rowIndex][colIndex].className = "clicked";
    tableData[rowIndex][colIndex].val = mineCount;

    if (mineCount==0) { 
      for (var i=Math.max(rowIndex-1,0); i<=Math.min(rowIndex+1,this.state.inputGrid-1); i++) {
        for(var j=Math.max(colIndex-1,0); j<=Math.min(colIndex+1,this.state.inputGrid-1); j++) {
          if (tableData[i][j].val==-1) this.clickNonMines(i,j,tableData);
        }
      }
    }

    return tableData;
  }

  showMines = (event) => {
    this.revealAll();
    this.setState({
      gameOver: true
    },()=>{
      alert("Game over !!!!")
    });
  }

  gameOverEle = () => {
    return (
      <>
        <h1>Sorry you clicked on mine</h1>
        <button onClick={() => { this.setState({ gameOver: false, tableData:this.initialise() }) }}>Try Again</button>
      </>
    )
  }

  revealAll = () =>{
    let tableData = clone(this.state.tableData);
    map(tableData,(rowData,rowIndex) => {
      map(rowData,(colData,colIndex) => {
        if(colData.isMines ){
          colData.className+=" clicked";
        }
      })
    });

    return tableData;
  }

  checkWinner = () =>{
    let isWinner = true;
    map(this.state.tableData,(rowData,rowIndex) => {
      map(rowData,(colData,colIndex) => {
        if(colData.val === -1 && !colData.isMines ){
          isWinner=false;
        }
      })
    });

    if (isWinner) {
     let tableData= this.revealAll();
     this.setState({
       isWinner:true,
       tableData
     })
    }
  }

  initTable = () => {
    let mines = this.state.mines;
    let tableData = this.state.tableData;
    console.log(tableData)
    return (
      <table ref={(ref)=> this.tableRef = ref} className={this.state.isTest?"test":""}>
        <tbody>
          {map(tableData,(rowData,rowIndex) => (
            <tr index={rowIndex}>
              {map(rowData,(colData,colIndex) => {
                return colData.isMines ? this.getMinesEle(rowIndex, colIndex,colData) :
              <td index={rowIndex + "" + colIndex} onClick={this.cellClick} data-index={[rowIndex, colIndex]} className={colData.className}>{colData.val}</td>
              })}
            </tr>
          ))}
        </tbody>
      </table>
    )
  }

  setLevelsConfig = (e,{value}) =>{
    let numberOfMines=20;
    let inputGrid = 10;
    console.log(typeof value);
    if(value=="Simple"){
      numberOfMines=10;
      inputGrid=5;
    }else if(value == "Medium"){
      numberOfMines=40;
      inputGrid=10;
    }else if(value == "Hard"){
      numberOfMines=100;
      inputGrid=20;
    }

    this.setState({
      isIntialise:true,
      numberOfMines,
      inputGrid,
      isWinner:false
    },()=>{
      this.setState({
        tableData:this.initialise()
      })
    });
  }

  selectLevel = ()=>{
    const level = [
      { key: 'simple', value: 'Simple', text: 'Simple' },
      { key: 'medium', value: 'Medium', text: 'Medium' },
      { key: 'hard', value: 'Hard', text: 'Hard' }
    ]
    
    return  (
      <Segment>
        <Select placeholder='Select level' options={level} onChange={this.setLevelsConfig}/>
      </Segment>
    )
    
  }

  render() {
    return <>
      {this.selectLevel()}
      {this.state.isIntialise?this.initTable():null}
      {this.state.gameOver ? this.gameOverEle() : null}
      {this.state.isWinner?<h1>Hey you are the winner</h1>:null}
    </>
  }
}